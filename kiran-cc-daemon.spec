Name: 		kiran-cc-daemon
Version:	2.5.1
Release:	4
Summary:	DBus daemon for Kiran Desktop

License:	MulanPSL-2.0
Source0:	%{name}-%{version}.tar.gz
Patch0:     0001-fix-translate-add-some-translation.patch
Patch0001:	0001-fix-keyboard-Add-modifier-lock-window-tips-enable-ke.patch
Patch0002:	0001-fix-audio-Fix-coredump-problem-caused-by-nullpointer.patch
Patch0003:	0001-fix-audio-Fix-the-type-of-return-value-in-template.patch


BuildRequires:	cmake >= 3.2
BuildRequires:	pkgconfig(glibmm-2.4)
BuildRequires:	pkgconfig(giomm-2.4)
BuildRequires:	pkgconfig(gmodule-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gtkmm-3.0)
BuildRequires:	pkgconfig(libxml++-2.6)
BuildRequires:	systemd-devel
BuildRequires:  libselinux-devel
BuildRequires:  gettext
BuildRequires:	gcc-c++
BuildRequires:	intltool
BuildRequires:	libX11-devel
BuildRequires:	xerces-c-devel
BuildRequires:	xsd
BuildRequires:	fontconfig-devel
BuildRequires:	jsoncpp-devel
BuildRequires:	cairomm-devel
BuildRequires:  kiran-log-gtk3-devel
BuildRequires:	fmt-devel >= 6.2.1
BuildRequires:  python%{python3_pkgversion}-jinja2
BuildRequires:	gdbus-codegen-glibmm
BuildRequires:  gtest-devel
BuildRequires:  libnotify-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  libgtop2-devel
BuildRequires:  libgudev-devel
BuildRequires:  cryptopp-devel


%description
DBus daemon for Kiran Desktop


%package -n kiran-system-daemon
Summary:    System DBus daemon for Kiran Desktop
Requires:   systemd
Requires:   dbus-daemon
Requires:   polkit
Requires:   kiran-cc-daemon-common
Requires:   util-linux
Requires:   pciutils
Requires:   libgtop2
Requires:   libgudev
Requires:   lshw
Requires:   passwd
Requires:   iso-codes


%if "%{?kylin}" == ""
Requires:   openeuler-lsb
%else
Requires:   kylin-lsb
%endif

%description -n kiran-system-daemon
System DBus daemon for Kiran Desktop

%package -n kiran-session-daemon
Summary:    Session DBus daemon for Kiran Desktop
Requires:   dbus-daemon
Requires:   kiran-session-manager
Requires:   xorg-x11-server-utils
Requires:   kiran-cc-daemon-common
Requires:   upower
Requires:   pulseaudio

%description -n kiran-session-daemon
Session DBus daemon for Kiran Desktop

%package common
Summary:    Common files for kiran-session-daemon and kiran-system-daemon

%description common
Common files for kiran-session-daemon and kiran-system-daemon

%package devel
Summary:    Development files for communicating with control center daemon

%description devel
Development files for communicating with control center daemon

%prep
%autosetup -p1

%build
%cmake
make %{?_smp_mflags}

%install
%make_install

%post -n kiran-system-daemon
%systemd_post kiran-system-daemon.service
systemctl enable kiran-system-daemon.service

%preun -n kiran-system-daemon
%systemd_preun kiran-system-daemon.service

%post -n kiran-session-daemon
glib-compile-schemas /usr/share/glib-2.0/schemas &> /dev/nulls || :

%files -n kiran-system-daemon
%{_sysconfdir}/dbus-1/system.d/*.conf
%{_sysconfdir}/lightdm/kiran-greeter.conf
%config(noreplace) %{_sysconfdir}/kiran-cc-daemon/system/timedate/timedate.conf
%{_bindir}/kiran-system-daemon
%{_usr}/lib/systemd/system/*.service
%{_libdir}/kiran-cc-daemon/system/*.so
%{_libdir}/kiran-cc-daemon/system/plugin_options
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/polkit-1/actions/com.kylinsec.Kiran.SystemDaemon*.policy
%{_datadir}/lightdm/lightdm.conf.d/99-kiran-greeter-login.conf

%files -n kiran-session-daemon
%{_sysconfdir}/xdg/autostart/kiran-session-daemon.desktop
%{_sysconfdir}/xdg/autostart/kiran-power-status-icon.desktop
%{_bindir}/kiran-session-daemon
%{_bindir}/kiran-power-backlight-helper
%{_bindir}/kiran-power-status-icon
%{_libdir}/kiran-cc-daemon/session/*.so
%{_libdir}/kiran-cc-daemon/session/plugin_options
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/dbus-1/services/*.service
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/polkit-1/actions/com.kylinsec.Kiran.SessionDaemon*.policy
%dir %{_datadir}/kiran-cc-daemon/keybindings
%{_datadir}/kiran-cc-daemon/keybindings/*.xml
%{_datadir}/kiran-cc-daemon/kiran-session-daemon.gresource
%dir %{_datadir}/kiran-cc-daemon/images
%{_datadir}/kiran-cc-daemon/images/*.png

%files common
%{_includedir}/kiran-cc-daemon/error-i.h
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-cc-daemon.mo

%files devel
%dir %{_includedir}/kiran-cc-daemon
%dir %{_includedir}/kiran-cc-daemon/kiran-system-daemon
%dir %{_includedir}/kiran-cc-daemon/kiran-session-daemon
%{_includedir}/kiran-cc-daemon/kiran-system-daemon/*-i.h
%{_includedir}/kiran-cc-daemon/kiran-session-daemon/*-i.h
%{_libdir}/pkgconfig/kiran-cc-daemon.pc

%changelog
* Sun Apr 23 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.1-4
- KYOS-F: Conditional statement error

* Fri Apr 14 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.1-3
- KYOS-B: Add modifier lock window tips enable key with gsettings (#67766)
- KYOS-B: Fix coredump problem caused by nullpointer to string
- KYOS-B: Fix the type of return value in template

* Mon Apr 10 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.1-2
- KYOS-T: add some translation

* Wed Mar 29 2023 tangjie02 <tangjie02@kylinsec.com.cn> - 2.5.1-1
- KYOS-F: Add some power functions. (#68459)

* Fri Mar 24 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.0-1
- KYOS-F: init commit for v2.5 version.

